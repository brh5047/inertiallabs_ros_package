/**
 * \cond INCLUDE_PRIVATE
 * \file
 *
 * \section LICENSE
 * MIT License (MIT)
 *
 * Copyright (c) 2018 Inertial Labs, Inc(TM)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * \section DESCRIPTION
 * This file implements the functions for interfacing with a Inertial Labs device.
 */


#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdint.h>
#include <bits/stdio2.h>
#include "InertialLabs_INS.h"
#include "IL_errorCodes.h"


#define COMMAND_HEADER_SIZE				5
#define RESPONSE_BUILDER_BUFFER_SIZE	256
#define READ_BUFFER_SIZE				256
#define IL_MAX_COMMAND_SIZE				10
#define IL_MAX_RESPONSE_SIZE			256
#define IL_RESPONSE_MATCH_SIZE			10
#define NUMBER_OF_MILLISECONDS_TO_SLEEP_AFTER_NOT_RECEIVING_ON_COM_PORT 	5
#define DEFAULT_TIMEOUT_IN_MS			5000



typedef struct {
/**
	 * Handle to the comPortServiceThread.
	 */
	IL_HANDLE				comPortServiceThreadHandle;

	/**
	 * Handle to the COM port.
	 */
	IL_HANDLE				comPortHandle;


	IL_HANDLE				waitForThreadToStopServicingComPortEvent;
	IL_HANDLE				waitForThreadToStartServicingComPortEvent;

	/**
	 * Used by the user thread to wait until the comPortServiceThread receives
	 * a command response.
	 */
	IL_HANDLE				waitForCommandResponseEvent;

	IL_CRITICAL_SECTION		criticalSection;

	/**
	 * Critical section for communicating over the COM port.
	 */
	IL_CRITICAL_SECTION		critSecForComPort;

	/**
	 * Critical section for accessing the response match fields.
	 */
	IL_CRITICAL_SECTION		critSecForResponseMatchAccess;

	/**
	 * Critical section for accessing the latestAsyncData field.
	 */
	IL_CRITICAL_SECTION		critSecForLatestAsyncDataAccess;

	/**
	 * Signals to the comPortServiceThread if it should continue servicing the
	 * COM port.
	 */
	IL_BOOL					continueServicingComPort;

	/**
	 * This field is used to signal to the comPortServiceThread that it should
	 * be checking to a command response comming from the INS device. The
	 * user thread can toggle this field after setting the cmdResponseMatch
	 * field so the comPortServiceThread differeniate between the various
	 * output data of the INS. This field should only be accessed in the
	 * functions INS_shouldCheckForResponse_threadSafe,
	 * INS_enableResponseChecking_threadSafe, and
	 * INS_disableResponseChecking_threadSafe.
	 */
	IL_BOOL					checkForResponse;

	/**
	 * This field contains the string the comPortServiceThread will use to
	 * check if a data packet from the INS is a match for the command sent.
	 * This field should only be accessed in the functions
	 * INS_shouldCheckForResponse_threadSafe, INS_enableResponseChecking_threadSafe,
	 * and INS_disableResponseChecking_threadSafe.
	 */
	char					cmdResponseMatchBuffer[IL_RESPONSE_MATCH_SIZE + 1];

	/**
	 * This field is used by the comPortServiceThread to place responses
	 * received from commands sent to the INS device. The responses
	 * placed in this buffer will be null-terminated and of the form
	 * "INS_SensorsData" where the checksum is stripped since this will
	 * have already been checked by the thread comPortServiceThread.
	 */
	char					cmdResponseBuffer[IL_MAX_RESPONSE_SIZE + 1];

	unsigned char*       	dataBuffer;
	//unsigned char           dataBuffer[READ_BUFFER_SIZE];
	int 			        cmdResponseSize;

	int 					num_bytes_recive;
	int 					recive_flag;

	INSCompositeData		lastestAsyncData;

	/**
	 * This field specifies the number of milliseconds to wait for a response
	 * from sensor before timing out.
	 */
	int						timeout;

	/**
	 * Holds pointer to a listener for async data recieved.
	 */
	INSNewDataReceivedListener DataListener;

} INSInternal;

/* Private type definitions. *************************************************/


void* INS_communicationHandler(void*);

IL_ERROR_CODE  INS_writeOutCommand(IL_INS* ins, unsigned char cmdToSend[]);


/**
 * \brief Indicates whether the comPortServiceThread should be checking
 * incoming data packets for matches to a command sent out.
 *
 * \param[in]	ins	Pointer to the IL_INS control object.
 *
 * \param[in ]  curResponsePos
 * The size of the packet going to recive 
 * 
 * \param[out]	responseMatchBuffer
 * \\TODO : add the comment , not sure now 
 *  
 * \return IL_TRUE if response checking should be performed; Il_FALSE if no
 * checking should be performed.
 */
IL_BOOL INS_shouldCheckForResponse_threadSafe(IL_INS* ins, char* responseMatchBuffer ,  int num_bytes_to_recive);

/**
 * \brief Enabled response checking by the comPortServiceThread.
 *
 * \param[in]	ins	Pointer to the IL_INS control object.
 *
 * \param[in]	responseMatch
 * \\ TODO: add the comments here 
 */
void INS_enableResponseChecking_threadSafe(IL_INS* ins, int responseMatch);

/**
 * \brief Disable response checking by the comPortServiceThread.
 *
 * \param[in]	ins 	Pointer to the IL_INS control object.
 */
void INS_disableResponseChecking_threadSafe(IL_INS* ins);

/**
 * \brief Performs a send command and then receive response transaction.
 *
 * Takes a command  and transmits it to the device.
 * The function will then wait until the response is received. The response
 * will be located in the INSInternal->cmdResponseBuffer field and will be
 * null-terminated.
 *
 * \param[in]	ins				Pointer to the IL_INS control object.
 *
 * \param[in]	responseMatch
 * The size of the data recive & latter need to check the checksum 
 *
 * \param[in]	cmdToSend
 * Pointer to the command data to transmit to the device. Should be
 * null-terminated.
 * 
 * \return InertialLabs error code.
 */
IL_ERROR_CODE INS_transaction(IL_INS* ins, unsigned char cmdToSend[], int responseMatch);


/**
 * \brief Sends out data over the connected COM port in a thread-safe manner.
 *
 * Sends out data over the connected COM port in a thread-safe manner to avoid
 * conflicts between the comPortServiceThread and the user thread. Use only the
 * functions  inertial_writeData_threadSafe and inertial_readData_threadSafe to ensure
 * communcation over the COM port is thread-safe.
 */
int INS_writeData_threadSafe(IL_INS* ins, unsigned char dataToSend[], unsigned int dataLength);


/**
 * \brief Reads data from the connected COM port in a thread-safe manner.
 *
 * Reads data from the connected COM port in a thread-safe manner to avoid
 * conflicts between the comPortServiceThread and the user thread. Use only the
 * functions inertial_writeData_threadSafe and inertial_readData_threadSafe to ensure
 * communcation over the COM port is thread-safe.
 */
IL_ERROR_CODE INS_readData_threadSafe(IL_INS* ins, unsigned char dataBuffer[], unsigned int numOfBytesToRead, unsigned int* numOfBytesActuallyRead);

/**
 * \brief Helper method to get the internal data of a INS control object.
 *
 * \param[in]	INS	Pointer to the INS control object.
 * \return The internal data.
 */
INSInternal* INS_getInternalData(IL_INS* ins);

void INS_processAsyncData(IL_INS* ins, unsigned char buffer[]);

/**
 * \brief  Process and Check the received packaet and transfer in to the internal structure.
 *
 * \param[in]	INS	Pointer to the INS control object.
 * \param[in]   data buffer 
 * \param[in]   Number of bytes recieved from the INS
 * 
 * \return Copy the data buffer to internal data structure variable.
 */
void INS_processReceivedPacket(IL_INS* ins, unsigned char buffer[] , int num_bytes_to_recive);

/**
 * \brief Helper method to set the buffer size in the INS control object,according to the command.
 *
 * \param[in]	INS	Pointer to the INS control object.
 * \return buffer size in  internal data structure.
 */
void INS_Recive_size(IL_INS* ins);


/* Function definitions. *****************************************************/


IL_ERROR_CODE INS_connect(IL_INS* newINS, const char* portName, int baudrate)
{

	INSInternal* INSInt;
	IL_ERROR_CODE errorCode;

	/* Allocate memory. */
	INSInt = (INSInternal*) malloc(sizeof(INSInternal));
	newINS->internalData = INSInt;

	newINS->portName = (char*) malloc(strlen(portName) + 1);
	strcpy(newINS->portName, portName);
	newINS->baudRate = baudrate;
	newINS->isConnected = IL_FALSE;
	INSInt->continueServicingComPort = IL_TRUE;
	INSInt->checkForResponse = IL_FALSE;
	INSInt->timeout = DEFAULT_TIMEOUT_IN_MS;
	INSInt->DataListener = NULL;

	memset(&INSInt->lastestAsyncData, 0, sizeof(INSCompositeData));

	
	errorCode = inertial_comPort_open(&INSInt->comPortHandle, portName, baudrate);

	if (errorCode != ILERR_NO_ERROR)
		return errorCode;

	errorCode = inertial_criticalSection_initialize(&INSInt->criticalSection);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
	errorCode = inertial_criticalSection_initialize(&INSInt->critSecForComPort);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
	errorCode = inertial_criticalSection_initialize(&INSInt->critSecForResponseMatchAccess);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
	errorCode = inertial_criticalSection_initialize(&INSInt->critSecForLatestAsyncDataAccess);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;

	errorCode = inertial_event_create(&INSInt->waitForThreadToStopServicingComPortEvent);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
	errorCode = inertial_event_create(&INSInt->waitForCommandResponseEvent);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
	errorCode = inertial_event_create(&INSInt->waitForThreadToStartServicingComPortEvent);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
	
	errorCode = inertial_thread_startNew(&INSInt->comPortServiceThreadHandle, &INS_communicationHandler, newINS);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;
		
	newINS->isConnected = IL_TRUE;

	errorCode = inertial_event_waitFor(INSInt->waitForThreadToStartServicingComPortEvent, -1);
	if (errorCode != ILERR_NO_ERROR)
		return errorCode;

	printf("last step \n");
	return ILERR_NO_ERROR;
}


IL_ERROR_CODE INS_disconnect(IL_INS* ins)
{
	INSInternal* INSInt;

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	INSInt = INS_getInternalData(ins);

	INSInt->continueServicingComPort = IL_FALSE;

	inertial_event_waitFor(INSInt->waitForThreadToStopServicingComPortEvent, -1);
	INS_Stop(ins);
	//sleep(3);

	inertial_comPort_close(INSInt->comPortHandle);

	inertial_criticalSection_dispose(&INSInt->criticalSection);
	inertial_criticalSection_dispose(&INSInt->critSecForComPort);
	inertial_criticalSection_dispose(&INSInt->critSecForResponseMatchAccess);
	inertial_criticalSection_dispose(&INSInt->critSecForLatestAsyncDataAccess);
	

	/* Free the memory associated with the INS structure. */
	//free(&INSInt->waitForThreadToStopServicingComPortEvent);
	//free(&INSInt->waitForCommandResponseEvent);
	//free(&INSInt->waitForThreadToStartServicingComPortEvent);
	free(ins->internalData);

	ins->isConnected = IL_FALSE;

	sleep(3);
	return ILERR_NO_ERROR;
}


int INS_writeData_threadSafe(IL_INS* ins, unsigned char dataToSend[], unsigned int dataLength)
{
	int errorCode;
	INSInternal* INSInt;

	INSInt = INS_getInternalData(ins);

	#if IL_DBG
	printf("INS_writeData_threadSafe : 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x  \n", dataToSend[0] ,dataToSend[1] ,dataToSend[2],
																					dataToSend[3] ,dataToSend[4],dataToSend[5],
																					dataToSend[6] ,dataToSend[7],dataToSend[8]);
	
	#endif 
	inertial_criticalSection_enter(&INSInt->critSecForComPort);
	errorCode = inertial_comPort_writeData(INSInt->comPortHandle, dataToSend, dataLength);
	inertial_criticalSection_leave(&INSInt->critSecForComPort);

	return errorCode;
}

IL_ERROR_CODE INS_readData_threadSafe(IL_INS* ins, unsigned char dataBuffer[], unsigned int numOfBytesToRead, unsigned int* numOfBytesActuallyRead)
{
	IL_ERROR_CODE errorCode;
	INSInternal* INSInt;

	INSInt = INS_getInternalData(ins);

	inertial_criticalSection_enter(&INSInt->critSecForComPort);
	errorCode = inertial_comPort_readData(INSInt->comPortHandle, dataBuffer, numOfBytesToRead, numOfBytesActuallyRead);
	inertial_criticalSection_leave(&INSInt->critSecForComPort);

	if(ins->mode)
	{
		
		if(	(ins->cmd_flag == IL_OPVT_RECEIVE         && *numOfBytesActuallyRead == (int)IL_OPVT_CMD_RECEIVE_SIZE    )           ||
			(ins->cmd_flag == IL_QPVT_RECEIVE         && *numOfBytesActuallyRead == (int)IL_QPVT_CMD_RECEIVE_SIZE )              ||
			(ins->cmd_flag == IL_OPVT2A_RECEIVE       && *numOfBytesActuallyRead == (int)IL_OPVT2A_CMD_RECEIVE_SIZE )            ||
			(ins->cmd_flag == IL_OPVT2AW_RECEIVE      && *numOfBytesActuallyRead == (int)IL_OPVT2AW_CMD_RECEIVE_SIZE )           ||
			(ins->cmd_flag == IL_OPVT2AHR_RECEIVE     && *numOfBytesActuallyRead == (int)IL_OPVT2AHR_CMD_RECEIVE_SIZE )          ||
			(ins->cmd_flag == IL_OPVTAD_RECEIVE       && *numOfBytesActuallyRead == (int)IL_OPVTAD_CMD_RECEIVE_SIZE )            ||
			(ins->cmd_flag == IL_MINIMAL_DATA_RECEIVE && *numOfBytesActuallyRead == (int)IL_MINIMAL_DATA_CMD_RECEIVE_SIZE ))
		{
			return ILERR_NO_ERROR ;
		}
		else if (ins->cmd_flag == IL_STOP_CMD && *numOfBytesActuallyRead == IL_STOP_CMD_RECEIVE_SIZE )
		{
	
			return ILERR_NO_ERROR ;
		}
		else
		{ 
			return ILERR_RECIVE_SIZE_ERROR;
		
		}
	}

	return errorCode;
	
}

IL_ERROR_CODE INS_writeOutCommand(IL_INS* ins, unsigned char *cmdToSend)
{
	//char packetTail[] = {0xFF ,0xFF};
	// printf("0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x  %d \n", cmdToSend[0] ,cmdToSend[1] ,cmdToSend[2],
	// 																				cmdToSend[3] ,cmdToSend[4],cmdToSend[5],
	// 																				cmdToSend[6] ,cmdToSend[7],cmdToSend[8], sizeof(cmdToSend));
	INS_writeData_threadSafe(ins, cmdToSend, sizeof(cmdToSend)+1 );

	//printf("size of : %d ",strlen(cmdToSend));
	//INS_writeData_threadSafe(ins,packetTail, strlen(packetTail));

	return ILERR_NO_ERROR;
}


void* INS_communicationHandler(void* INSobj)
{
	
	IL_INS* ins;
	INSInternal* INSInt;
	unsigned char responseBuilderBuffer[RESPONSE_BUILDER_BUFFER_SIZE];
	unsigned int responseBuilderBufferPos = 0;
	unsigned char readBuffer[READ_BUFFER_SIZE];
	unsigned int numOfBytesRead = 0;
	IL_BOOL haveFoundStartOfCommand = IL_FALSE;

	memset( readBuffer, 0x00, sizeof( unsigned char ) * READ_BUFFER_SIZE );

	ins  = (IL_INS*) INSobj;
	INSInt = INS_getInternalData(ins);

	inertial_event_signal(INSInt->waitForThreadToStartServicingComPortEvent);

	ins->read_flag = 0;

	while (INSInt->continueServicingComPort) {

		unsigned int curResponsePos = 0;

		INS_readData_threadSafe(ins, readBuffer, READ_BUFFER_SIZE, &numOfBytesRead);

		
		if (numOfBytesRead == 0 )
		{
			// There was no data. Sleep for a short amount of time before continuing.
			
			inertial_sleepInMs(NUMBER_OF_MILLISECONDS_TO_SLEEP_AFTER_NOT_RECEIVING_ON_COM_PORT);
			///if(ins->cmd_flag > 0)
			//{
			//	printf("baud rate issue!!");
			//	break;
			//}
			//printf("num read 0");
			continue;
		}
		else
		{
			//ins->read_flag = 1;
			#if IL_DBG
			for(int i = 0 ; i< numOfBytesRead;i++)
			{
				printf("0x%02x  ", readBuffer[i]);
			}
			printf("\n");
			printf("numOfBytesRead %d : \n", numOfBytesRead);
			printf("number of bytes to recive : %d \n" , INSInt->num_bytes_recive);
			printf(" cmd_flag : %d \n" ,ins->cmd_flag);
			#endif 

			switch(ins->cmd_flag)
			{
				case IL_STOP_CMD:
					printf("IL_STOP_CMD \n");
					break;

				case IL_SET_ONREQUEST_CMD:
					printf("IL_SET_ONREQUEST_CMD \n");
					break;

				case IL_OPVT_RECEIVE:
				case IL_QPVT_RECEIVE:
				case IL_OPVT2A_RECEIVE:
				case IL_OPVT2AW_RECEIVE:
				case IL_OPVT2AHR_RECEIVE:
				case IL_OPVTAD_RECEIVE:
				case IL_MINIMAL_DATA_RECEIVE:
				{ 
					//printf("cmd_flag value : %d \n" , ins->cmd_flag);
					if(ins->mode)
					{
						ins->read_flag = 1;
						//inertial_criticalSection_enter(&INSInt->critSecForResponseMatchAccess);
						INS_processReceivedPacket(ins,readBuffer,INSInt->num_bytes_recive);
						//inertial_criticalSection_leave(&INSInt->critSecForResponseMatchAccess);
					}
					else
					{
						for ( ; curResponsePos < numOfBytesRead; curResponsePos++) {

							if (responseBuilderBufferPos > RESPONSE_BUILDER_BUFFER_SIZE) {
							/* We are about to overfill our buffer. Let's just reinitialize the buffer. */
								printf(" We are about to overfill our buffer \n");
								haveFoundStartOfCommand = IL_FALSE;
								responseBuilderBufferPos = 0;
							}
							/* See if we have even found the start of a response. */
							if (((int)readBuffer[curResponsePos] == 170 ) && ((int)readBuffer[curResponsePos+1] == 85 ) ) {
				
								
								printf(" found the start byte \n");
								#if IL_DBG
									for(int i = 0 ; i< numOfBytesRead;i++)
									{
										printf("0x%02x  ", readBuffer[i]);
									}
									printf("\n");
								#endif
								/* Alright, we have found the start of a command. */
								//free(INSInt->dataBuffer);
								haveFoundStartOfCommand = IL_TRUE;
								responseBuilderBufferPos = 0;
								
							}

							if (haveFoundStartOfCommand ) {

								responseBuilderBuffer[responseBuilderBufferPos] = readBuffer[curResponsePos];
								responseBuilderBufferPos++;
								if(responseBuilderBufferPos == INSInt->num_bytes_recive)
								{
									haveFoundStartOfCommand = IL_FALSE;
								}
							}

							if(responseBuilderBufferPos == INSInt->num_bytes_recive)
							{
								printf(" one packet recive done in continues mode \n");
								ins->read_flag = 1;
								//inertial_criticalSection_enter(&INSInt->critSecForResponseMatchAccess);
								INS_processReceivedPacket(ins,responseBuilderBuffer,INSInt->num_bytes_recive);
								responseBuilderBufferPos = 0;
								haveFoundStartOfCommand = IL_FALSE;
								//inertial_criticalSection_leave(&INSInt->critSecForResponseMatchAccess);


							}
							else {
								/* We received data but we have not found the starting point of a command. */
								
							}
						}	
					}
					
					break;
				}
				case IL_READ_INS_PAR_RECEIVE:
				{
					printf("here \n");
					ins->read_flag = 1;
					INS_processReceivedPacket(ins,readBuffer,INSInt->num_bytes_recive);	
					break;
				}
				default:
					printf("no cmd flag set \n");
					break;
			}
	
		}
	}
	inertial_event_signal(INSInt->waitForThreadToStopServicingComPortEvent);

	return IL_NULL;

}


IL_BOOL INS_verifyConnectivity(IL_INS* ins )
{
  //TODO 
  return true;
}
void textFromHexString(char *hex,char *result)
{
    char text[25]={0}; 
    int tc=0;
                     
    for(int k=0;k<strlen(hex);k++)
    {
        if(k%2!=0)
        {
            char temp[3];
            sprintf(temp,"%c%c",hex[k-1],hex[k]);
            int number = (int)strtol(temp, NULL, 16);
            text[tc]=(char)(number);
                        
            tc++;   
                         
        }
    }  
    strcpy(result,text);
          
}

IL_ERROR_CODE INS_ReadInternalParameters(IL_INS* ins , INSSetInternalData* data)
{

	int i = 6 ;

	int m_conv = 100;
	int deg_conv = 10000000;

		printf("here  try to recive");

	INSInternal* INSInt;

	if (!ins->read_flag)
	{
		printf("no data recived in the buffer , please check the baud rate for the module \n");
		return ILERR_MEMORY_ERROR;	
	}
	
	INSInt = INS_getInternalData(ins);

	//printf("0x%04x", *(uint16_t*)(&(INSInt->dataBuffer[i+0])));



	data->Data_rate             =(double)( *(uint16_t*)(&(INSInt->dataBuffer[i+0])) ) ;

	data->Initial_Alignment_Time=(double)( *(uint16_t*)(&(INSInt->dataBuffer[i+2])) ) ;

	data->Magnetic_Declination  =(double)( *(int32_t*)(&(INSInt->dataBuffer[i+4])) )/m_conv ;

	data->Latitude              =(double)( *(int32_t*)(&(INSInt->dataBuffer[i+8])) )/deg_conv ;

	data->Longitude             =(double)( *(int32_t*)(&(INSInt->dataBuffer[i+12])) )/deg_conv ;

	data->Altitude              =(double)( *(int32_t*)(&(INSInt->dataBuffer[i+16])) )/m_conv ;

	data->Year                  =(int)( *(int8_t*)(&(INSInt->dataBuffer[i+20])) ) ;

	data->Month                 =(int)( *(int8_t*)(&(INSInt->dataBuffer[i+21])) ) ;

	data->Day                   =(int)( *(int8_t*)(&(INSInt->dataBuffer[i+22])) ) ;

	data->Alignment_Angle_A1    =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+23])) )/m_conv ;

	data->Alignment_Angle_A2    =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+25])) )/m_conv ;

	data->Alignment_Angle_A3    =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+27])) )/m_conv ;

	data->INS_Mount_Right      =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+29])) )/m_conv ;

	data->INS_Mount_Forward    =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+31])) )/m_conv ;

	data->INS_Mount_Up         =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+33])) )/m_conv ;

	data->Antenna_Pos_Right    =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+35])) )/m_conv ;

	data->Antenna_Pos_Forward =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+37])) )/m_conv ;

	data->Antenna_Pos_Up       =(double)( *(int16_t*)(&(INSInt->dataBuffer[i+39])) )/m_conv ;

    
	data->Altitude_one_byte   =(int)( *(int8_t*)(&(INSInt->dataBuffer[i+41])) );


	data->Baro_Altimeter      = (int)( *(int8_t*)(&(INSInt->dataBuffer[i+58])) );

	
	//ROS_INFO("\nINS Device Name: %s\n",data.INS_Device_Name);

	printf("\nDate:  %d - %d - %d\n",data->Year,data->Month,data->Day);

	printf("\nData Rate: %f\n",data->Data_rate);

	printf(
	     "\n Initial Alignment Time:  %f\n", 
	        data->Initial_Alignment_Time);

	printf(
	       "\nMagnetic declination, Mdec : %f\n",
	         data->Magnetic_Declination);

	printf(
	       "\n Latitude : %f\n"
	       "  Longitude : %f\n"
	       "  Altitude  : %f\n",
	          data->Latitude,
	          data->Longitude,
	          data->Altitude);

    printf(
	       "\n INS_Mount_Right : %f\n"
	       "  INS_Mount_Forward : %f\n"
	       "  INS_Mount_Up  : %f\n",
	          data->INS_Mount_Right,
	          data->INS_Mount_Forward,
	          data->INS_Mount_Up);

    printf(
	       "\n Alignment_Angle_A1 : %f\n"
	       "  Alignment_Angle_A2 : %f\n"
	       "  Alignment_Angle_A3  : %f\n",
	          data->Alignment_Angle_A1,
	          data->Alignment_Angle_A2,
	          data->Alignment_Angle_A3);

    printf(
	       "\n Antenna_Pos_Right : %f\n"
	       "  Antenna_Pos_Forward : %f\n"
	       "  Antenna_Pos_Up  : %f\n",
	          data->Antenna_Pos_Right,
	          data->Antenna_Pos_Forward,
	          data->Antenna_Pos_Up);

    printf( "\n Baro_altimeter : %d \n " , data->Baro_Altimeter);

	return ILERR_NO_ERROR;
	
}


INSInternal* INS_getInternalData(IL_INS* ins)
{
	return (INSInternal*) ins->internalData;
}

IL_ERROR_CODE INS_transaction(IL_INS* ins, unsigned char *cmdToSend, int responseMatch)
{
	INSInternal* INSInt;

	INSInt = INS_getInternalData(ins);

	
	INS_enableResponseChecking_threadSafe(ins, responseMatch);

	INS_writeData_threadSafe(ins, cmdToSend, sizeof(cmdToSend));

	return inertial_event_waitFor(INSInt->waitForCommandResponseEvent, INSInt->timeout);
}

void INS_enableResponseChecking_threadSafe(IL_INS* ins, int responseMatch)
{
	INSInternal* INSInt;

	INSInt = INS_getInternalData(ins);

	inertial_criticalSection_enter(&INSInt->critSecForResponseMatchAccess);

	INSInt->checkForResponse = IL_TRUE;
	INSInt->cmdResponseSize = responseMatch;

	inertial_criticalSection_leave(&INSInt->critSecForResponseMatchAccess);
}


void INS_processReceivedPacket(IL_INS* ins, unsigned char buffer[], int num_bytes_to_recive)
{
	INSInternal* INSInt;

	
	INSInt = INS_getInternalData(ins);

	inertial_criticalSection_enter(&INSInt->critSecForLatestAsyncDataAccess);
	INSInt->dataBuffer = buffer;
	inertial_criticalSection_leave(&INSInt->critSecForLatestAsyncDataAccess);
	
	for(int i =0;i<INSInt->num_bytes_recive;i++)
	{
		printf("0x%02x " , INSInt->dataBuffer[i]);
	}
	printf("\n");

#if 0
	/* See if we should be checking for a command response. */
	if (INS_shouldCheckForResponse_threadSafe(ins, buffer , num_bytes_to_recive)) {
		
		/* We should be checking for a command response. */

		/* Does the data packet match the command response we expect? */
		//if (strncmp(responseMatch, buffer, strlen(responseMatch)) == 0) {

			/* We found a command response match! */

			/* If everything checks out on this command packet, let's disable
			 * further response checking. */
			//INS_disableResponseChecking_threadSafe(ins);

			/* The line below should be thread-safe since the user thread should be
			 * blocked until we signal that we have received the response. */
			

			/* Signal to the user thread we have received a response. */
			inertial_event_signal(INSInt->waitForCommandResponseEvent);
		//}
	}
#endif 

}

IL_ERROR_CODE INS_YPR(IL_INS* ins , INSCompositeData* data)
{
	int i ;
	INSInternal* INSInt;

	printf("inside INS_YPR\n");


	if (!ins->read_flag)
	{
		printf("no data recived in the buffer , please check the baud rate for the module \n");
		return ILERR_MEMORY_ERROR;	
	}

	INSInt = INS_getInternalData(ins);

	switch (ins->cmd_flag)
	{
		case IL_OPVT_RECEIVE:
		case IL_OPVT2A_RECEIVE:
		case IL_OPVT2AW_RECEIVE:
		case IL_OPVT2AHR_RECEIVE:
		case IL_OPVTAD_RECEIVE:
		case IL_MINIMAL_DATA_RECEIVE:
		{
			i = 6 ;

			data->ypr.yaw = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+0])) ) / 100;
			data->ypr.pitch = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) / 100;
			data->ypr.roll = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4])) ) / 100;

			break;
		}
		default:
		{
			return ILERR_NO_ERROR;
			printf("YPR (Heading , Pitch , Roll)  not supported in this data output format!!! \n");
			break;
		}
	}

}

IL_ERROR_CODE INS_getGyroAccMag( IL_INS* ins , INSCompositeData* data)
{
	IL_ERROR_CODE errorCode ;
	int i ;
	int kg = 50 ;
	int ka = 4000;
	INSInternal* INSInt;

	if (!ins->read_flag)
	{
		printf("no data recived in the buffer , please check the baud rate for the module \n");
		return ILERR_MEMORY_ERROR;	
	}

	INSInt = INS_getInternalData(ins);

	switch(ins->cmd_flag)
	{		
		case IL_OPVT_RECEIVE:
		case IL_QPVT_RECEIVE:
		case IL_OPVT2A_RECEIVE:
		case IL_OPVT2AW_RECEIVE:
		{
			errorCode =  ILERR_MEMORY_ERROR;
			i = (ins->cmd_flag == IL_QPVT_RECEIVE ) ? 14 : 12 ;

			data->gyro.c0 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) / kg;
			data->gyro.c1 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) / kg;
			data->gyro.c2 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4])) ) / kg;


			i = i+6 ;

			data->acceleration.c0 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) / ka;
			data->acceleration.c1 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) / ka;
			data->acceleration.c2 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4])) ) / ka;

			i = i+6 ;

		    data->magnetic.c0 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) *10;
			data->magnetic.c1 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) *10;
			data->magnetic.c2 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4])) ) *10;

			i = i+8 ; 

			data->Vinp = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+0])) ) /100;
			data->Temper = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) /10;

			break;
		}
		case IL_OPVT2AHR_RECEIVE:
		case IL_OPVTAD_RECEIVE:
		{
			i = 12;
			

			data->gyro.c0 = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+0])) ) / 100000;
			data->gyro.c1 = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+4])) ) / 100000;
			data->gyro.c2 = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+8])) ) / 100000;

			i = 24 ;
			
			data->acceleration.c0 = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+0])) ) / 1000000;
			data->acceleration.c1 = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+4])) ) / 1000000;
			data->acceleration.c2 = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+8])) ) / 1000000;

			i = 36 ;

			data->magnetic.c0 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) *10;
			data->magnetic.c1 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) *10;
			data->magnetic.c2 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4])) ) *10;
			
			i = 44 ; 


			data->Vinp = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+0])) ) /100;
			data->Temper = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) /10;

			break;
		}
		default :
		{
			printf("Gyro , Accleration , Magnetic  not supported in this data output format!!! \n");
			return ILERR_NO_ERROR;
			break;
		}
	}
}

IL_ERROR_CODE INS_getPositionData( IL_INS* ins , INSPositionData* data)
{
	int errorCode ;
	int i =0;int j = 0;
    int deg_conv = 10000000;
	int deg_conv_8byte = 1000000000;
	int m_conv   = 100;
	INSInternal* INSInt;

	if (!ins->read_flag)
	{
		printf("no data recived in the buffer , please check the baud rate for the module \n");
		return ILERR_MEMORY_ERROR;	
	}

	INSInt = INS_getInternalData(ins);

	switch(ins->cmd_flag)
	{
		case IL_OPVT2A_RECEIVE:
		case IL_OPVT2AW_RECEIVE:
		case IL_QPVT_RECEIVE:
		case IL_OPVT_RECEIVE:
		{
			//i = 36 ; j = 60;
			errorCode =  ILERR_MEMORY_ERROR;
			i = (ins->cmd_flag == IL_QPVT_RECEIVE ) ? 38 : 36;
			j = i+24;

			data->Latitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+0])) ) / deg_conv;
			data->Longitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+4])) ) / deg_conv;
			//data->Altitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+8])) ) / m_conv;
			data->East_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+12])) ) / m_conv;
			data->North_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+16])) ) / m_conv;
			data->Vertical_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+20])) ) / m_conv;

			data->GNSS_Latitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+0])) ) / deg_conv;
			data->GNSS_Longitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+4])) ) / deg_conv;
			data->GNSS_Altitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+8])) ) / m_conv;
			data->Altitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+8])) ) / m_conv;
			data->GNSS_Horizontal_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+12])) ) / m_conv;
			data->GNSS_Trackover_Ground = (double)( *(uint16_t*)(&(INSInt->dataBuffer[j+16])) ) / m_conv;
			data->GNSS_Vertical_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+18])) ) / m_conv;


			break;
		}
		case IL_OPVT2AHR_RECEIVE:
		case IL_OPVTAD_RECEIVE:
		{
			errorCode =  ILERR_MEMORY_ERROR;
			i = 48 ;
			j = i+32;

			data->Latitude = (double)( *(int64_t*)(&(INSInt->dataBuffer[i+0])) ) / deg_conv_8byte;
			data->Longitude = (double)( *(int64_t*)(&(INSInt->dataBuffer[i+8])) ) / deg_conv_8byte;
			//data->Altitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+16])) ) / 1000;
			data->East_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+20])) ) / m_conv;
			data->North_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+24])) ) / m_conv;
			data->Vertical_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+28])) ) / m_conv;

			data->GNSS_Latitude = (double)( *(int64_t*)(&(INSInt->dataBuffer[j+0])) ) / deg_conv_8byte;
			data->GNSS_Longitude = (double)( *(int64_t*)(&(INSInt->dataBuffer[j+8])) ) / deg_conv_8byte;
			data->GNSS_Altitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+16])) ) / 1000;
			data->Altitude = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+16])) ) / 1000;
			data->GNSS_Horizontal_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+20])) ) / m_conv;
			data->GNSS_Trackover_Ground = (double)( *(uint16_t*)(&(INSInt->dataBuffer[j+24])) ) / m_conv;
			data->GNSS_Vertical_Speed = (double)( *(int32_t*)(&(INSInt->dataBuffer[j+26])) ) / m_conv;

			break;
		}
		default:
		{
			printf("Position data  not supported in this data output format!!! \n");
			return ILERR_NO_ERROR;
			break;
		}

	}

}

IL_ERROR_CODE INS_getQuaternionData( IL_INS* ins , INSCompositeData* data)
{
	int i; 
    int quat_conv = 10000;
	INSInternal* INSInt;

	printf("inside INS_getQuaternionData\n");

	if (!ins->read_flag)
	{
		printf("no data recived in the buffer , please check the baud rate for the module \n");
		return ILERR_MEMORY_ERROR;	
	}
	
	INSInt = INS_getInternalData(ins);

	switch (ins->cmd_flag)
	{

		case IL_QPVT_RECEIVE:
		{
			i = 6;

			data->quaternion.Lk0 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) / quat_conv;
			data->quaternion.Lk1 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2])) ) / quat_conv;
			data->quaternion.Lk2 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4])) ) / quat_conv;
			data->quaternion.Lk3 = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+6])) ) / quat_conv;

			break;
		}
		default:
		{
			printf("Quaternion are not supported in this data output format!!! \n");
			return ILERR_NO_ERROR;
			break;
		}
	}

}

IL_ERROR_CODE INS_getPressureBarometricData( IL_INS* ins , INSCompositeData* data)
{
	int errorCode ;
	int i ; 
    int Hbar_conv = 100;
	int Pbar_conv = 2 ;
	INSInternal* INSInt;

	printf("inside INS_getPressureBarometricData\n");

	INSInt = INS_getInternalData(ins);

	switch (ins->cmd_flag)
	{
		case IL_OPVT_RECEIVE:
		case IL_QPVT_RECEIVE:
		{
			i = (ins->cmd_flag == IL_OPVT_RECEIVE ) ? 91 : 93 ;

			
			data->P_bar = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) * Pbar_conv;
			data->H_bar = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+2])) ) / Hbar_conv;

			break;
		}
		case IL_OPVT2A_RECEIVE:
		case IL_OPVT2AW_RECEIVE:
		case IL_OPVT2AHR_RECEIVE:
		case IL_OPVTAD_RECEIVE:
		{

			i = (ins->cmd_flag == IL_OPVT2A_RECEIVE ) ? 100 : (ins->cmd_flag == IL_OPVT2AW_RECEIVE ) ? 102 : (ins->cmd_flag == IL_OPVT2AHR_RECEIVE ) ? 128 : 124;
			

			data->P_bar = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])) ) * Pbar_conv;
			data->H_bar = (double)( *(int32_t*)(&(INSInt->dataBuffer[i+2])) ) / Hbar_conv;


			break;
		}
		default:
		{
			printf("Pressure Barometric data not supported in this data output format!!! \n");
			return ILERR_NO_ERROR;
			break;
		}
	}
}

IL_ERROR_CODE INS_getLatencyData(IL_INS* ins , INSPositionData* data )
{
	IL_ERROR_CODE errorCode;
	int i; 
	INSInternal* INSInt;

	printf("inside INS_getQuaternionData\n");

	INSInt = INS_getInternalData(ins);

	switch (ins->cmd_flag)
	{
		case IL_OPVT_RECEIVE:
		{
			i = 89;

			data->Latency_ms_pos = (double)( *(int8_t*)(&(INSInt->dataBuffer[i+0])) );
			data->Latency_ms_vel = (double)( *(int8_t*)(&(INSInt->dataBuffer[i+1])) ) ;
	
			break;
		}
		case IL_QPVT_RECEIVE:
		case IL_OPVTAD_RECEIVE:
		case IL_OPVT2AW_RECEIVE:
		{

			i = (ins->cmd_flag == IL_OPVTAD_RECEIVE ) ? 117 : 91 ;

			data->Latency_ms_pos = (double)( *(int8_t*)(&(INSInt->dataBuffer[i+0])));
			data->Latency_ms_vel = (double)( *(int8_t*)(&(INSInt->dataBuffer[i+1]))) ;

			break;
		}
		case IL_OPVT2AHR_RECEIVE:
		case IL_OPVT2A_RECEIVE:
		{

			i = (ins->cmd_flag == IL_OPVT2A_RECEIVE ) ? 94 : 122 ;

			data->Latency_ms_pos = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+0])));
			data->Latency_ms_vel = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2]))) ;
			data->Latency_ms_vel = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+4]))) ;

			break;
		}
		default:
		{
			printf("Latency ms_head, Latency ms_pos, and Latency ms_vel are not supported in this data output format!!! \n");
			return ILERR_NO_ERROR;
			break;
		}
	}
}

IL_ERROR_CODE INS_getGNSS_HeadPitch(IL_INS* ins ,  INSPositionData* data)
{
	int errorCode;
	int i;
    int gnss_conv = 100;
	INSInternal* INSInt;

	printf("inside INS_getGNSS_HeadPitch \n");

	INSInt = INS_getInternalData(ins);

	switch (ins->cmd_flag)
	{
		case IL_OPVT2AW_RECEIVE:
		{

			i = 94;

			data->GNSS_Heading = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+0])))/gnss_conv;
			data->GNSS_Pitch = (double)( *(int16_t*)(&(INSInt->dataBuffer[i+2]))) /gnss_conv;
			data->GNSS_Heading_STD = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+4]))) /gnss_conv;
            data->GNSS_Pitch_STD = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+6]))) /gnss_conv;


			break;
		}
		case IL_OPVTAD_RECEIVE:
		case IL_OPVT2AHR_RECEIVE:
		case IL_OPVT2A_RECEIVE:
		{
			i = (ins->cmd_flag == IL_OPVT2A_RECEIVE ) ? 92 : 120 ;
			
			data->GNSS_Heading = (double)( *(uint16_t*)(&(INSInt->dataBuffer[i+0])))/gnss_conv;

			break;
		}
		default:
		{
			printf("Heading GNSS or Pitch GNSS or Heading STD GNSS or Pitch STD GNSS not supported for this data output format\n");
			return ILERR_NO_ERROR;
			break;
		}
	}
}
IL_ERROR_CODE INS_SetMode(IL_INS* ins,int mode)
{
	ins->mode = mode;
	unsigned int syncDataOutputType  = 100;
	IL_BOOL waitForResponse = 1;
	if(ins->mode)
	{
		ins->cmd_flag = IL_SET_ONREQUEST_CMD;
		INS_setSetOnRequestMode(ins,syncDataOutputType , waitForResponse );
		return ILERR_NO_ERROR;
	}
	else
	{ 
		printf("now you can call datamode functions you want to recive from the INS ");
		sleep(2);
		return ILERR_NO_ERROR;
	}
}

IL_ERROR_CODE INS_Stop(IL_INS* ins)
{
	int errorCode;
	printf("inside INS_Stop \n");
	unsigned char stop_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0xFE ,0x05, 0x01};

	ins->cmd_flag = IL_STOP_CMD;
	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, stop_payload);
	return errorCode;
}

IL_ERROR_CODE INS_setSetOnRequestMode(IL_INS* ins, unsigned int syncDataOutputType, IL_BOOL waitForResponse)
{
	 
	int errorCode;
	unsigned char  setonrequest_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0xC1 ,0xC8 ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_setSetOnRequestMode\n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, setonrequest_payload);

	printf("inside INS_setSetOnRequestMode done\n");
	return errorCode;
}

IL_ERROR_CODE INS_OPVTdata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  OPVTdata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x52 ,0x59 ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_OPVTdata_Recive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, OPVTdata_payload);

	return errorCode;
}

IL_ERROR_CODE INS_QPVTdata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  QPVTdata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x56 ,0x5D ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_QPVTdata_Receive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, QPVTdata_payload);

	return errorCode;
}

IL_ERROR_CODE INS_OPVT2Adata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  OPVT2Adata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x57 ,0x5E ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_OPVT2Adata_Receive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, OPVT2Adata_payload);

	return errorCode;
}

IL_ERROR_CODE INS_OPVT2AWdata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  OPVT2AWdata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x59 ,0x60 ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_OPVT2AWdata_Receive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, OPVT2AWdata_payload);

	return errorCode;
}

IL_ERROR_CODE INS_OPVT2AHRdata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  OPVT2AHRdata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x58 ,0x5F,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_OPVT2AHRdata_Receive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, OPVT2AHRdata_payload);

	return errorCode;
}

IL_ERROR_CODE INS_OPVTADdata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  OPVTADdata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x61 ,0x68 ,0x00};


	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_OPVTADdata_Receive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, OPVTADdata_payload);

	return errorCode;	
}

IL_ERROR_CODE INS_Minimaldata_Receive(IL_INS* ins)
{
	int errorCode;
	unsigned char  Minimaldata_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x53 ,0x5A ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_Minimaldata_Receive \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, Minimaldata_payload);

	return errorCode;	
}

IL_ERROR_CODE INS_DevSelfTest(IL_INS* ins)
{
	int errorCode;
	unsigned char  DevSelfTest_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x13 ,0x1A ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_DevSelfTest \n");

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, DevSelfTest_payload);

	return errorCode;
}

IL_ERROR_CODE INS_ReadINSpar(IL_INS* ins)
{
	int errorCode;
	//INSSetInternalData *data;
	unsigned char  ReadINSpar_payload[] = {0XAA, 0x55, 0x00, 0x00, 0x07,0x00,0x41 ,0x48 ,0x00};

	if (!ins->isConnected)
		return ILERR_NOT_CONNECTED;

	printf("inside INS_ReadINSpar \n");

	ins->cmd_flag = IL_READ_INS_PAR_RECEIVE;

	INS_Recive_size(ins);
	errorCode = INS_writeOutCommand(ins, ReadINSpar_payload);

	//sleep(2);
	printf("here\n");
	//errorCode = INS_ReadInternalParameters(ins,data);
	printf("inside INS_ReadINSpar -2 \n");
	return errorCode;
}

void INS_Recive_size(IL_INS* ins)
{
	INSInternal* INSInt;

	INSInt = INS_getInternalData(ins);

	printf("now ins->cmd_flag : %d" , ins->cmd_flag);
	switch(ins->cmd_flag)
	{
		case IL_STOP_CMD:
			INSInt->num_bytes_recive = IL_STOP_CMD_RECEIVE_SIZE ;
			break;
		case IL_SET_ONREQUEST_CMD:
			INSInt->num_bytes_recive = IL_SET_ONREQUEST_CMD_RECEIVE_SIZE ;
			break;
		case IL_OPVT_RECEIVE:
			INSInt->num_bytes_recive = IL_OPVT_CMD_RECEIVE_SIZE ;
			break;
		case IL_QPVT_RECEIVE:
			INSInt->num_bytes_recive = IL_QPVT_CMD_RECEIVE_SIZE ;
			break;
		case IL_OPVT2A_RECEIVE:
			INSInt->num_bytes_recive = IL_OPVT2A_CMD_RECEIVE_SIZE;
			break;
		case IL_OPVT2AW_RECEIVE:
			INSInt->num_bytes_recive = IL_OPVT2AW_CMD_RECEIVE_SIZE;
			break;
		case IL_OPVT2AHR_RECEIVE:
			INSInt->num_bytes_recive = IL_OPVT2AHR_CMD_RECEIVE_SIZE;
			break;
		case IL_OPVTAD_RECEIVE:
			INSInt->num_bytes_recive = IL_OPVTAD_CMD_RECEIVE_SIZE;
			break;
		case IL_MINIMAL_DATA_RECEIVE:
			INSInt->num_bytes_recive = IL_MINIMAL_DATA_CMD_RECEIVE_SIZE;
			break;
		case IL_DEV_SELF_TEST_RECEIVE:
			INSInt->num_bytes_recive = IL_DEV_SELF_TEST_CMD_RECEIVE_SIZE;
			break;
		case IL_READ_INS_PAR_RECEIVE:
		    INSInt->num_bytes_recive = IL_READ_INS_PAR_CMD_RECEIVE_SIZE;
			break;
		default:
			break;
	}
}

IL_BOOL INS_shouldCheckForResponse_threadSafe(IL_INS* ins, char* responseMatchBuffer , int num_bytes_to_recive)
{
	INSInternal* INSInt;
	IL_BOOL shouldCheckResponse;

	INSInt = INS_getInternalData(ins);

	inertial_criticalSection_enter(&INSInt->critSecForResponseMatchAccess);
	shouldCheckResponse = 1 ; // to avoid warning now
	inertial_criticalSection_leave(&INSInt->critSecForResponseMatchAccess);

	return shouldCheckResponse;
}


void INS_disableResponseChecking_threadSafe(IL_INS* ins)
{
	INSInternal* INSInt;

	INSInt = INS_getInternalData(ins);

	inertial_criticalSection_enter(&INSInt->critSecForResponseMatchAccess);

	INSInt->checkForResponse = IL_FALSE;
	INSInt->cmdResponseSize = 0 ;
	INSInt->cmdResponseMatchBuffer[0] = 0;

	inertial_criticalSection_leave(&INSInt->critSecForResponseMatchAccess);
}


IL_ERROR_CODE INS_registerDataReceivedListener(IL_INS* ins, INSNewDataReceivedListener listener)
{
	INSInternal* INSInt = INS_getInternalData(ins);

	if (INSInt->DataListener != NULL)
		return ILERR_UNKNOWN_ERROR;

	INSInt->DataListener = listener;

	return ILERR_NO_ERROR;
}

IL_ERROR_CODE INS_unregisterDataReceivedListener(IL_INS* ins, INSNewDataReceivedListener listener)
{
	INSInternal* INSInt = INS_getInternalData(ins);

	if (INSInt->DataListener == NULL)
		return ILERR_UNKNOWN_ERROR;

	if (INSInt->DataListener != listener)
		return ILERR_UNKNOWN_ERROR;

	INSInt->DataListener = NULL;

	return ILERR_NO_ERROR;
}

unsigned char INS_checksum_compute(const char* cmdToCheck)
{
	int i;
	unsigned char xorVal = 0;
	int cmdLength;

	cmdLength = strlen(cmdToCheck);

	for (i = 0; i < cmdLength; i++)
		xorVal ^= (unsigned char) cmdToCheck[i];

	return xorVal;
}

void INS_checksum_computeAndReturnAsHex(const char* cmdToCheck, char* checksum)
{
	unsigned char cs;
	char tempChecksumHolder[3];

	cs = INS_checksum_compute(cmdToCheck);
	sprintf(tempChecksumHolder, "%X", cs);

	checksum[0] = tempChecksumHolder[0];
	checksum[1] = tempChecksumHolder[1];
}

void INS_processAsyncData(IL_INS* ins, unsigned char buffer[] )
{
	INSCompositeData data;
	INSInternal* INSInt = INS_getInternalData(ins);

	memset(&data, 0, sizeof(INSCompositeData));


	/* We had an async data packet and need to move it to INSInt->lastestAsyncData. */
	inertial_criticalSection_enter(&INSInt->critSecForLatestAsyncDataAccess);
	memcpy(&INSInt->lastestAsyncData, &data, sizeof(INSCompositeData));
	inertial_criticalSection_leave(&INSInt->critSecForLatestAsyncDataAccess);

	if (INSInt->DataListener != NULL)
		INSInt->DataListener(ins, &INSInt->lastestAsyncData);


}
#endif

/** \endcond */