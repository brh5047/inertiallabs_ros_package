# InertialLabs_ros_package


[![alt text](https://readthedocs.org/projects/docs/badge/?version=latest "Documentation Status")](https://gitlab.com/oblivione/inertiallabs_ros_package)


![Picture of IMU](https://inertiallabs.com/static/assets/img/products/INS-D.jpg)

The `InertialLabs_ros_package` package is a linux ROS driver for GPS-Aided Inertial Navigation Systems (INS) of [Inertial Labs](https://inertiallabs.com/). The package is developed based on the official [SDK v0.1](https://gitlab.com/oblivione/inertiallabs_ins_sdk) for Linux. The user manual for the device can be found [here](https://inertiallabs.com/static/pdf/INS-Datasheet.rev3.2_Nov_2018.pdf).

The package is tested on Ubuntu 16.04 LTS & 18.04 LTS  with ROS Kinetic & ROS Melodic . You can install ROS from [here](http://wiki.ros.org/kinetic/Installation/Ubuntu). 

## License

* The license for the official SDK is the MIT license which is included in the `ins_ros/inertiallabs_ins_sdk`
* The license for the other codes is Apache 2.0 whenever not specified.

## Compiling

This is a Catkin package. Make sure the package is on `ROS_PACKAGE_PATH` after cloning the package to your workspace. And the normal procedure for compiling a catkin package will work.

```
$ git colne https://gitlab.com/oblivione/inertiallabs_ros_package
$ cd inertiallabs_ros_package
$ mv ins_ros <your_workspace_path>
$ cd <your_work_space>
$ catkin_make
$ source devel/setup.bash

```

**Node**

With the provided rosnode 

for on request mode 
```
 rosrun ins_ros ins_node _serial_baud:=460800 _ins_output_format:=3 

```

for continues mode 
```
 rosrun ins_ros ins_node_mode _serial_baud:=460800 _ins_output_format:=3 

```


## Example Usage

**Parameters**

`serial_port` (`string`, `default: /dev/ttyUSB0`)

Port which the device connected to. This can be checked by command `dmesg`.

`serial_baud` (`int`, `460800`)

The baud rate of the serial port. The available baud rates can be checked on the user manual. It is suggested that the baud rate is kept at `460800` to ensure the high frequency transmission. The device will send `permission denied` error code if the baud rate cannot support the desired data package at the desired frequency.The sdk supports 7 baud rates.


`ins_output_format` (`int`, `2`)

The output data format of the IMU data.

```
 IL_OPVT_RECEIVE 	 	2      
 IL_QPVT_RECEIVE      		3     
 IL_OPVT2A_RECEIVE    		4      
 IL_OPVT2AW_RECEIVE   		5      
 IL_OPVT2AHR_RECEIVE  		6       
 IL_OPVTAD_RECEIVE    		7    
 IL_MINIMAL_DATA_RECEIVE 	8 

```


**Published Topics**

`/Inertial_Labs/sensor_data` (`ins_ros/sensor_data`)
 
Publish Gyro(x,y,z) , Accelation(x,y,z) , Magnetic (x,y,z) , Temprature , Input Voltage , Pressure , Barometric height .

`/Inertial_Labs/ins_data` (`ins_ros/ins_data`)
 
 Publish Heading , Pitch , Roll values .

`/Inertial_Labs/gps_data` (`ins_ros/gps_data`)

 Publish Latitute, Longitute , Altitude , East Speed , North Speed  Vertial Speed values .

`/Inertial_Labs/quat_data` (`ins_ros/ins_data`)

 Publish  Quaternion of orientation values  . Only published in IL_QPVT_RECEIVE data output type . So the commmand line input will be _ins_output_format:=3 . 

 **Message Types**

  ins_data

  ```
  std_msgs/Header           header
  geometry_msgs/Vector3     YPR
  float32[4]                quat_data

  ```

  gps_data

  ```
  std_msgs/Header           header
  float64                   Latitude
  float64                   Longitude
  float64                   Altitude
  float32                   East_Speed
  float32                   North_Speed
  float32                   Vertical_Speed

  ```
  sensor_data

  ```
  std_msgs/Header           header
  geometry_msgs/Vector3     Mag
  geometry_msgs/Vector3     Accel
  geometry_msgs/Vector3     Gyro
  float32                   Temp
  float32                   Vinp
  float32                   Pressure
  float32                   Barometric_Height

  ```

## FAQ

1. The driver can't open my device?\
Make sure you have ownership of the device in `/dev`.

2. Why I have permission error during the initialization process of the driver?\
Most often, this is because the baud rate you set does not match the package size to be received. Try increase the baud rate.

3. Why is the IMU data output rate much lower than what is set?\
This may be due to a recent change in the FTDI USB-Serial driver in the Linux kernel, the following shell script might help:
    ```bash
    # Reduce latency in the FTDI serial-USB kernel driver to 1ms
    # This is required due to https://github.com/torvalds/linux/commit/c6dce262
    for file in $(ls /sys/bus/usb-serial/devices/); do
      value=`cat /sys/bus/usb-serial/devices/$file/latency_timer`
      if [ $value -gt 1 ]; then
        echo "Setting low_latency mode for $file"
        sudo sh -c "echo 1 > /sys/bus/usb-serial/devices/$file/latency_timer"
      fi
    done
    ```

## Bug Report

Prefer to open an issue. You can also send an E-mail to omprakashpatro@gmail.com.

